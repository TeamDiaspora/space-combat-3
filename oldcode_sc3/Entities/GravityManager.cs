﻿using System.Collections.Generic;
using Sandbox;
using SpaceCombat3.Interfaces;

namespace SpaceCombat3.Entities;

public partial class GravityManager : Entity
{
	private static List<IGravitySource> GravitySources = new List<IGravitySource>();
	private static List<IGravity> Recipients = new List<IGravity>();
	
	public static void RegisterSource(IGravitySource Source)
	{
		GravitySources.Add(Source);
	}

	public static void UnregisterSource(IGravitySource Source)
	{
		GravitySources.Remove(Source);
	}

	public static void RegisterRecipient(IGravity Recipient)
	{
		Recipients.Add(Recipient);
	}

	public static void UnregisterRecipient(IGravity Recipient)
	{
		Recipients.Remove(Recipient);
	}

	[Event.Tick.Client]
	[Event.Physics.PostStep]
	void OnPhysicsStep()
	{
		foreach (var Source in GravitySources)
		{
			var GravityCenter = Source.GetGravityCenter();
			var GravityRadius = Source.GetGravityRadius() * Source.GetGravityRadius();

			foreach (var Recipient in Recipients)
			{
				if (GravityCenter.DistanceSquared(Recipient.GetGravityPosition()) < GravityRadius && Source.ShouldAffectGravityRecipient(Recipient))
				{
					Source.CalculateGravityForRecipient(Recipient, out var GravityStrength, out var GravityDirection);
					Recipient.ApplyGravity(GravityDirection, GravityStrength);
				}
			}
		}
	}
}
