﻿using System.Xml.Schema;
using SpaceCombat3.Interfaces;

namespace SpaceCombat3.Entities;

public partial class Prop : Sandbox.Prop, IGravity
{
	public override void Spawn()
	{
		base.Spawn();
		
		GravityManager.RegisterRecipient(this);
	}

	protected override void OnDestroy()
	{
		base.OnDestroy();
		
		GravityManager.UnregisterRecipient(this);
	}

	public void ApplyGravity(Vector3 GravityDirection, float GravityStrength)
	{
		PhysicsBody.ApplyForce(GravityDirection * GravityStrength * PhysicsBody.Mass);
	}

	public Vector3 GetGravityPosition()
	{
		return Position;
	}
}
