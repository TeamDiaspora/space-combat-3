﻿using Sandbox;
using Sandbox.Utility;

namespace SpaceCombat3.NodeBus;

/*
NodeBus Data Frame
8 header
	- 1 start
	- 1 srr
	- 1 ide
	- 1 exec
	- 4 reserved
16 identifier A
	- 3 priority
	- 4 request type
	- 9 source device id
16 identifier B
	- 9 target device id
	- 7 target device function
8 dlc
0 - 256 (32 bytes) data
32 crc
 */
public struct DataPacket
{
	public byte Header;
	public ushort IdentifierA;
	public ushort IdentifierB;
	public byte DataLength;
	public byte[] Data;
	public uint CRC32;
}

// 8 bit data header
public enum EStandardDataHeader : byte
{
	DataBroadcast = 0b10000000,
	DataResponse = 0b10100000,
	DataRequestAnyDevice = 0b11000000,
	DataRequestSpecificDevice = 0b11100000,
	ExecuteBroadcast = 0b10110000,
	ExecuteDeviceFunction = 0b11110000,
}

// 3 bit data priority
public enum EDataPriority : byte
{
	Lowest	   = 0b000,
	Low 	   = 0b001,
	MediumLow  = 0b010,
	Medium	   = 0b011,
	MediumHigh = 0b100,
	High	   = 0b101,
	Highest    = 0b110,
	Emergency  = 0b111
}

// 4 bit service identifier
public enum EStandardRequestType : byte
{
	NetworkControl   = 0b0000,
	DeviceControl    = 0b0001,
	ReadData	     = 0b0010,
	WriteData        = 0b0011,
	RequestTransfer  = 0b0100,
	BeginTransfer    = 0b0101,
	TransferData     = 0b0111,
	TransferFinished = 0b1000,
	SecurityAccess   = 0b1001,
	Authentication   = 0b1010,
}

public static class DataPacketBuilder
{
	public static int MaxDataLength => 32;

	public static ushort NewIdentifierA(EDataPriority Priority, EStandardRequestType RequestType, ushort SourceDeviceId)
	{
		return 0;
	}

	public static ushort NewIdentifierB(ushort TargetDeviceId, byte TargetDeviceFunction)
	{
		return 0;
	}

	public static DataPacket NewDataPacket(EStandardDataHeader Header, ushort Identifier, byte[] Data)
	{
		Assert.True(Data.Length <= MaxDataLength);

		var Packet = new DataPacket
		{
			Header = (byte)Header,
			IdentifierA = Identifier,
			IdentifierB = 0,
			DataLength = (byte)Data.Length,
			Data = Data,
			CRC32 = Crc32.FromBytes(Data)
		};

		return Packet;
	}
}
