﻿namespace SpaceCombat3.Interfaces;

public interface IGravitySource
{
	float GetGravityRadius();

	Vector3 GetGravityCenter();

	bool ShouldAffectGravityRecipient(IGravity Recipient);

	void CalculateGravityForRecipient(IGravity Recipient, out float GravityStrength, out Vector3 GravityDirection);
}
