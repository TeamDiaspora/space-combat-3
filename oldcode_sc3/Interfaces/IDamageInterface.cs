﻿namespace SpaceCombat3.Interfaces;

public interface IDamageInterface
{
	bool CanDamage(Sandbox.Entity Instigator);
}
