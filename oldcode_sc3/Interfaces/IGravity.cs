﻿namespace SpaceCombat3.Interfaces;

public interface IGravity
{
	void ApplyGravity(Vector3 GravityDirection, float GravityStrength);
	Vector3 GetGravityPosition();
}
