﻿using Sandbox;
using SpaceCombat3.Pawns.PawnControllers.AnimationControllers;
using SpaceCombat3.Pawns.PawnControllers.PlayerMovement;

namespace SpaceCombat3.Pawns;

public partial class PlayerPawn : BasePawn
{
	private TimeSince timeSinceDropped;
	private TimeSince timeSinceJumpReleased;

	private DamageInfo lastDamage;

	/// <summary>
	/// The clothing container is what dresses the citizen
	/// </summary>
	public ClothingContainer Clothing = new();

	/// <summary>
	/// Default init
	/// </summary>
	public PlayerPawn()
	{
		
	}

	public override void Spawn()
	{
		base.Spawn();
		
		Respawn();
	}

	/// <summary>
	/// Initialize using this client
	/// </summary>
	public PlayerPawn(Client cl) : this()
	{
		// Load clothing from client data
		Clothing.LoadFromClient(cl);
	}

	public void Respawn()
	{
		SetModel("models/citizen/citizen.vmdl");

		Controller = new PlayerGroundMovementController();
		Animator = new PlayerAnimator();

		if (DevController is NoclipController)
		{
			DevController = null;
		}

		EnableAllCollisions = true;
		EnableDrawing = true;
		EnableHideInFirstPerson = true;
		EnableShadowInFirstPerson = true;

		Clothing.DressEntity(this);

		/*Inventory.Add(new PhysGun(), true);
		Inventory.Add(new GravGun());
		Inventory.Add(new Tool());
		Inventory.Add(new Pistol());
		Inventory.Add(new Flashlight());
		Inventory.Add(new Fists());*/

		CameraMode = new FirstPersonCamera();

		//base.Respawn();
		
		Host.AssertServer();

		LifeState = LifeState.Alive;
		Health = 100;
		Velocity = Vector3.Zero;
		WaterLevel = 0;

		CreateHull();

		Game.Current?.MoveToSpawnpoint( this );
		ResetInterpolation();
	}

	public override void OnKilled()
	{
		base.OnKilled();

		if (lastDamage.Flags.HasFlag(DamageFlags.Vehicle))
		{
			Particles.Create("particles/impact.flesh.bloodpuff-big.vpcf", lastDamage.Position);
			Particles.Create("particles/impact.flesh-big.vpcf", lastDamage.Position);
			PlaySound("kersplat");
		}

		BecomeRagdollOnClient(Velocity, lastDamage.Flags, lastDamage.Position, lastDamage.Force,
			GetHitboxBone(lastDamage.HitboxIndex));

		Controller = null;

		EnableAllCollisions = false;
		EnableDrawing = false;

		CameraMode = new SpectateRagdollCamera();

		foreach (var child in Children)
		{
			child.EnableDrawing = false;
		}

		Inventory.DropActive();
		Inventory.DeleteContents();
	}

	public override void TakeDamage(DamageInfo info)
	{
		if (GetHitboxGroup(info.HitboxIndex) == 1)
		{
			info.Damage *= 10.0f;
		}

		lastDamage = info;

		TookDamage(lastDamage.Flags, lastDamage.Position, lastDamage.Force);

		base.TakeDamage(info);
	}

	[ClientRpc]
	public void TookDamage(DamageFlags damageFlags, Vector3 forcePos, Vector3 force)
	{
	}

	public override PawnController GetActiveController()
	{
		if (DevController != null) return DevController;

		return base.GetActiveController();
	}

	public override void Simulate(Client cl)
	{
		base.Simulate(cl);

		if (Input.ActiveChild != null)
		{
			ActiveChild = Input.ActiveChild;
		}

		if (LifeState != LifeState.Alive)
			return;

		var controller = GetActiveController();
		if (controller != null)
			EnableSolidCollisions = !controller.HasTag("noclip");

		TickPlayerUse();
		SimulateActiveChild(cl, ActiveChild);

		if (Input.Pressed(InputButton.View))
		{
			if (CameraMode is ThirdPersonCamera)
			{
				CameraMode = new FirstPersonCamera();
			}
			else
			{
				CameraMode = new ThirdPersonCamera();
			}
		}

		if (Input.Pressed(InputButton.Drop))
		{
			var dropped = Inventory.DropActive();
			if (dropped != null)
			{
				dropped.PhysicsGroup.ApplyImpulse(Velocity + EyeRotation.Forward * 500.0f + Vector3.Up * 100.0f, true);
				dropped.PhysicsGroup.ApplyAngularImpulse(Vector3.Random * 100.0f, true);

				timeSinceDropped = 0;
			}
		}

		if (Input.Released(InputButton.Jump))
		{
			if (timeSinceJumpReleased < 0.3f)
			{
				Game.Current?.DoPlayerNoclip(cl);
			}

			timeSinceJumpReleased = 0;
		}

		if (Input.Left != 0 || Input.Forward != 0)
		{
			timeSinceJumpReleased = 1;
		}
	}

	public override void StartTouch(Entity other)
	{
		if (timeSinceDropped < 1) return;

		base.StartTouch(other);
	}

	[ConCmd.Server("inventory_current")]
	public static void SetInventoryCurrent(string entName)
	{
		var target = ConsoleSystem.Caller.Pawn as Player;
		if (target == null) return;

		var inventory = target.Inventory;
		if (inventory == null)
			return;

		for (int i = 0; i < inventory.Count(); ++i)
		{
			var slot = inventory.GetSlot(i);
			if (!slot.IsValid())
				continue;

			if (slot.ClassName != entName)
				continue;

			inventory.SetActiveSlot(i, false);

			break;
		}
	}
	
	/// <summary>
	/// Create a physics hull for this player. The hull stops physics objects and players passing through
	/// the player. It's basically a big solid box. It also what hits triggers and stuff.
	/// The player doesn't use this hull for its movement size.
	/// </summary>
	public virtual void CreateHull()
	{
		CollisionGroup = CollisionGroup.Player;
		AddCollisionLayer(CollisionLayer.Player);
		SetupPhysicsFromAABB(PhysicsMotionType.Keyframed, new Vector3(-16, -16, 0), new Vector3(16, 16, 72));

		//var capsule = new Capsule( new Vector3( 0, 0, 16 ), new Vector3( 0, 0, 72 - 16 ), 32 );
		//var phys = SetupPhysicsFromCapsule( PhysicsMotionType.Keyframed, capsule );


		//	phys.GetBody(0).RemoveShadowController();

		// TODO - investigate this? if we don't set movetype then the lerp is too much. Can we control lerp amount?
		// if so we should expose that instead, that would be awesome.
		MoveType = MoveType.MOVETYPE_WALK;
		EnableHitboxes = true;
	}

	TimeSince timeSinceLastFootstep = 0;

	/// <summary>
	/// A foostep has arrived!
	/// </summary>
	public override void OnAnimEventFootstep(Vector3 pos, int foot, float volume)
	{
		if (LifeState != LifeState.Alive)
			return;

		if (!IsClient)
			return;

		if (timeSinceLastFootstep < 0.2f)
			return;

		volume *= FootstepVolume();

		timeSinceLastFootstep = 0;

		//DebugOverlay.Box( 1, pos, -1, 1, Color.Red );
		//DebugOverlay.Text( pos, $"{volume}", Color.White, 5 );

		var tr = Trace.Ray(pos, pos + Vector3.Down * 20)
			.Radius(1)
			.Ignore(this)
			.Run();

		if (!tr.Hit) return;

		tr.Surface.DoFootstep(this, tr, foot, volume);
	}

	public virtual float FootstepVolume()
	{
		return Velocity.WithZ(0).Length.LerpInverse(0.0f, 200.0f) * 0.2f;
	}
	
	[ClientRpc]
	private void BecomeRagdollOnClient( Vector3 velocity, DamageFlags damageFlags, Vector3 forcePos, Vector3 force, int bone )
	{
		var ent = new ModelEntity();
		ent.Position = Position;
		ent.Rotation = Rotation;
		ent.Scale = Scale;
		ent.MoveType = MoveType.Physics;
		ent.UsePhysicsCollision = true;
		ent.EnableAllCollisions = true;
		ent.CollisionGroup = CollisionGroup.Debris;
		ent.SetModel( GetModelName() );
		ent.CopyBonesFrom( this );
		ent.CopyBodyGroups( this );
		ent.CopyMaterialGroup( this );
		ent.TakeDecalsFrom( this );
		ent.EnableHitboxes = true;
		ent.EnableAllCollisions = true;
		ent.SurroundingBoundsMode = SurroundingBoundsType.Physics;
		ent.RenderColor = RenderColor;
		ent.PhysicsGroup.Velocity = velocity;

		ent.SetInteractsAs( CollisionLayer.Debris );
		ent.SetInteractsWith( CollisionLayer.WORLD_GEOMETRY );
		ent.SetInteractsExclude( CollisionLayer.Player | CollisionLayer.Debris );

		foreach ( var child in Children )
		{
			if ( !child.Tags.Has( "clothes" ) ) continue;
			if ( child is not ModelEntity e ) continue;

			var model = e.GetModelName();

			var clothing = new ModelEntity();
			clothing.SetModel( model );
			clothing.SetParent( ent, true );
			clothing.RenderColor = e.RenderColor;
			clothing.CopyBodyGroups( e );
			clothing.CopyMaterialGroup( e );
		}

		if ( damageFlags.HasFlag( DamageFlags.Bullet ) ||
			 damageFlags.HasFlag( DamageFlags.PhysicsImpact ) )
		{
			PhysicsBody body = bone > 0 ? ent.GetBonePhysicsBody( bone ) : null;

			if ( body != null )
			{
				body.ApplyImpulseAt( forcePos, force * body.Mass );
			}
			else
			{
				ent.PhysicsGroup.ApplyImpulse( force );
			}
		}

		if ( damageFlags.HasFlag( DamageFlags.Blast ) )
		{
			if ( ent.PhysicsGroup != null )
			{
				ent.PhysicsGroup.AddVelocity( (Position - (forcePos + Vector3.Down * 100.0f)).Normal * (force.Length * 0.2f) );
				var angularDir = (Rotation.FromYaw( 90 ) * force.WithZ( 0 ).Normal).Normal;
				ent.PhysicsGroup.AddAngularVelocity( angularDir * (force.Length * 0.02f) );
			}
		}

		//Corpse = ent;

		ent.DeleteAsync( 10.0f );
	}
}
