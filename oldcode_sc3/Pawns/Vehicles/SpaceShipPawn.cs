﻿using Sandbox;
using SpaceCombat3.Pawns.PawnControllers.VehicleMovement;

namespace SpaceCombat3.Pawns.Vehicles;

public class SpaceShipPawn : VehiclePawn
{
	public SpaceShipPawn()
	{
		Controller = new FlyingVehicleMovementController();
		
		SetModel("models/smallbridge/ships/hysteria_galapagos.vmdl");
		
		EnableAllCollisions = true;
		EnableDrawing = true;

		CameraMode = new ThirdPersonCamera();
		
		CollisionGroup = CollisionGroup.Player;
		EnableHitboxes = true;
		
		AddCollisionLayer(CollisionLayer.Player);

		SetupPhysicsFromModel(PhysicsMotionType.Dynamic);

		MoveType = MoveType.MOVETYPE_CUSTOM;
	}
}
