﻿using Sandbox;
using SpaceCombat3.Pawns.Vehicles;

namespace SpaceCombat3.Pawns.PawnControllers.VehicleMovement;

public partial class FlyingVehicleMovementController : PawnController
{
	[Net]
	public Vector3 AccelerationMult
	{
		get;
		set;
	} = new Vector3(1, 1, 1);

	[Net]
	public float AccelerationSpeed
	{
		get;
		set;
	} = 7000;

	[Net]
	public float MaxSpeed
	{
		get;
		set;
	} = 3500;

	[Net]
	public float Bounce
	{
		get;
		set;
	} = 0.25f;

	[Net]
	public float Size
	{
		get;
		set;
	} = 500.0f;

	public override void Simulate()
	{
		var AccelerationDir = (Vector3.OneX * Input.Forward) + (Vector3.OneY * Input.Left) +
		                      (Vector3.OneZ * (Input.Down(InputButton.Jump)
			                      ? 1
			                      : (Input.Down(InputButton.Duck) ? -1 : 0)));

		var Transform = new Transform(Position, Rotation);

		var LocalVelocity = Transform.NormalToLocal(Velocity);

		if (!AccelerationDir.IsNearZeroLength)
		{
			LocalVelocity =
				(LocalVelocity + (AccelerationDir * AccelerationMult * AccelerationSpeed * Time.Delta)).Clamp(-MaxSpeed,
					MaxSpeed);
		}

		if (AccelerationDir.x.AlmostEqual(0))
		{
			LocalVelocity.x = LocalVelocity.x.Approach(0, AccelerationSpeed * Time.Delta * AccelerationMult.x);
		}

		if (AccelerationDir.y.AlmostEqual(0))
		{
			LocalVelocity.y = LocalVelocity.y.Approach(0, AccelerationSpeed * Time.Delta * AccelerationMult.y);
		}

		if (AccelerationDir.z.AlmostEqual(0))
		{
			LocalVelocity.z = LocalVelocity.z.Approach(0, AccelerationSpeed * Time.Delta * AccelerationMult.z);
		}

		Velocity = Transform.NormalToWorld(LocalVelocity);
		WishVelocity = Velocity;

		Move(Velocity * Time.Delta);

		GroundEntity = null;
		BaseVelocity = Vector3.Zero;
		Rotation = Input.Rotation;
	}

	public void Move(Vector3 Delta, int Iterations = 0)
	{
		if (Iterations > 4)
			return;

		var Length = Delta.Length;
		var TargetPosition = Position + Delta;
		var TraceResult = Trace.Sphere(Size, Position, Position + (Velocity * Time.Delta))
			.HitLayer(CollisionLayer.All, false)
			.HitLayer(CollisionLayer.Solid, true)
			.HitLayer(CollisionLayer.GRATE, true)
			.HitLayer(CollisionLayer.PLAYER_CLIP, true)
			.HitLayer(CollisionLayer.WINDOW, true)
			.HitLayer(CollisionLayer.NPC, true)
			.Ignore(Pawn)
			.Run();

		if (TraceResult.StartedSolid)
		{
			Position = TargetPosition;
		}
		else if (TraceResult.Hit)
		{
			Position = TraceResult.EndPosition + TraceResult.Normal;

			// subtract the normal from our velocity
			Velocity = Velocity.SubtractDirection(TraceResult.Normal * (1.0f + Bounce));

			Delta = Velocity.Normal * Delta.Length * (1.0f - TraceResult.Fraction);

			Move(Delta, ++Iterations);
		}
		else
		{
			Position = TraceResult.EndPosition;
		}
	}
}
