﻿using Sandbox;

namespace SpaceCombat3.Pawns.PawnControllers.AnimationControllers;

public partial class PlayerAnimator : StandardPlayerAnimator
{
	[Net]
	public float TurnSpeed
	{
		get;
		set;
	} = 0.1f;

	[Net]
	public float DuckedTurnSpeed
	{
		get;
		set;
	} = 0.5f;
	
	protected TimeSince TimeSinceFootShuffle = 60;

	public override void Simulate()
	{
		base.Simulate();

		if (Pawn is PlayerPawn {ActiveChild: BaseCarriable Carry})
		{
			Carry.SimulateAnimator(this);
		}
		else
		{
			SetAnimParameter("holdtype", 0);
			SetAnimParameter("aim_body_weight", 0.5f);
		}
	}

	public override void FrameSimulate()
	{
		base.FrameSimulate();

		var IdealRotation = Rotation.LookAt(Input.Rotation.Forward.WithZ(0), Vector3.Up);
		DoRotation(IdealRotation);
	}

	public override void DoRotation(Rotation IdealRotation)
	{
		/*//
		// Our ideal player model rotation is the way we're facing
		//
		var AllowYawDiff = 0.1f;
		float WantedTurnSpeed = HasTag("ducked") ? DuckedTurnSpeed : TurnSpeed;

		//
		// If we're moving, rotate to our ideal rotation
		//
		Rotation = Rotation.Slerp(Rotation, IdealRotation, WishVelocity.Length * Time.Delta * WantedTurnSpeed);

		//
		// Clamp the foot rotation to within 120 degrees of the ideal rotation
		//
		Rotation = Rotation.Clamp(IdealRotation, AllowYawDiff, out var Change);

		//
		// If we did restrict, and are standing still, add a foot shuffle
		//
		if (Change > 1 && WishVelocity.Length <= 1) TimeSinceFootShuffle = 0;

		SetAnimParameter("b_shuffle", TimeSinceFootShuffle < 0.1);*/
	}
}
