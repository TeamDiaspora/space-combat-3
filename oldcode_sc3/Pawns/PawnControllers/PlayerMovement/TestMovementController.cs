﻿using System;
using System.Numerics;
using Sandbox;
using SpaceCombat3.Entities;
using SpaceCombat3.Interfaces;

namespace SpaceCombat3.Pawns.PawnControllers.PlayerMovement;

[Library]
public partial class TestMovementController : BasePlayerController, IGravity
{
	[Net]
	public float Gravity
	{
		get;
		set;
	} = 800f;

	[Net]
	public Vector3 GravityDirection
	{
		get;
		set;
	} = Vector3.Down;

	public Matrix4x4 WorldMatrix = Matrix4x4.Identity;
	
	public TestMovementController()
	{
		GravityManager.RegisterRecipient(this);
	}

	~TestMovementController()
	{
		GravityManager.UnregisterRecipient(this);
	}
	
	public override void Simulate()
	{
		var vel = (Input.Rotation.Forward * Input.Forward) + (Input.Rotation.Left * Input.Left);

		if ( Input.Down( InputButton.Jump ) )
		{
			vel += Vector3.Up * 1;
		}

		vel = vel.Normal * 2000;

		if ( Input.Down( InputButton.Run ) )
			vel *= 5.0f;

		if ( Input.Down( InputButton.Duck ) )
			vel *= 0.2f;

		Velocity += vel * Time.Delta;

		if ( Velocity.LengthSquared > 0.01f )
		{
			Position += Velocity * Time.Delta;
		}

		Velocity = Velocity.Approach( 0, Velocity.Length * Time.Delta * 5.0f );

		UpdatePlayerRotation();


		EyeRotation = Input.Rotation;
		WishVelocity = Velocity;
		GroundEntity = null;
		BaseVelocity = Vector3.Zero;

		SetTag( "noclip" );
	}
	
	Vector3 GetCapsuleAxisX()
	{
		// Fast simplification of FQuat::RotateVector() with FVector(1,0,0).
		Rotation CapsuleRotation = Rotation;
		Vector3 QuatVector = new Vector3(CapsuleRotation.x, CapsuleRotation.y, CapsuleRotation.z);

		return new Vector3(MathF.Pow(CapsuleRotation.w, 2f) - QuatVector.LengthSquared, CapsuleRotation.z * CapsuleRotation.w * 2.0f,
			CapsuleRotation.y * CapsuleRotation.w * -2.0f) + QuatVector * (CapsuleRotation.x * 2.0f);
	}
	
	Vector3 GetCapsuleAxisZ()
	{
		// Fast simplification of FQuat::RotateVector() with FVector(0,0,1).
		Rotation CapsuleRotation = Rotation;
		Vector3 QuatVector = new Vector3(CapsuleRotation.x, CapsuleRotation.y, CapsuleRotation.z);

		return new Vector3(CapsuleRotation.y * CapsuleRotation.w * 2.0f, CapsuleRotation.x * CapsuleRotation.w * -2.0f,
			MathF.Pow(CapsuleRotation.w, 2) - QuatVector.LengthSquared) + QuatVector * (CapsuleRotation.z * 2.0f);
	}
	
	Vector3 GetGravityDirection(bool AvoidZeroGravity)
	{
		// Gravity direction can be influenced by the custom gravity scale value.
		if (Gravity != 0.0f)
		{
			if (!GravityDirection.IsNearZeroLength)
			{
				return GravityDirection * ((Gravity > 0.0f) ? 1.0f : -1.0f);
			}

			if (AvoidZeroGravity)
			{
				return Vector3.Down;
			}
		}
		else if (AvoidZeroGravity)
		{
			if (!GravityDirection.IsNearZeroLength)
			{
				return GravityDirection;
			}

			return Vector3.Down;
		}

		return Vector3.Zero;
	}

	public void ApplyGravity(Vector3 GravityDirection, float GravityStrength)
	{
		if (GravityStrength <= 1f)
		{
			this.Gravity = 0f;
			return;
		}

		this.GravityDirection = GravityDirection;
		this.Gravity = GravityStrength;
	}

	public Vector3 GetGravityPosition()
	{
		return Position;
	}
	
	Matrix4x4 MakeFromZX(Vector3 ZAxis, Vector3 XAxis)
	{
		Vector3 NewZ = ZAxis.Normal;
		Vector3 Norm = XAxis.Normal;

		// if they're almost same, we need to find arbitrary vector
		if (MathF.Abs(NewZ.Dot(Norm)).AlmostEqual(1f))
		{
			// make sure we don't ever pick the same as NewX
			Norm = (MathF.Abs(NewZ.z) < (1f - 0.0001)) ? Vector3.OneZ : Vector3.OneX;
		}

		Vector3 NewY = NewZ.Cross(Norm).Normal;
		Vector3 NewX = NewY.Cross(NewZ);

		return new Matrix4x4(
			NewX.x, NewX.y, NewX.z, 0f,
			NewY.x, NewY.y, NewY.z, 0f,
			NewZ.x, NewZ.y, NewZ.z, 0f,
			0f, 0f, 0f, 1f
		);
	}
		
	private void UpdatePlayerRotation()
	{
		Vector3 DesiredUpDir = GetGravityDirection(true) * -1.0f;

		if (GetCapsuleAxisZ().Dot(DesiredUpDir) >= 0.999845f)
		{
			return;
		}

		WorldMatrix = MakeFromZX(DesiredUpDir, GetCapsuleAxisX());
		Matrix4x4.Decompose(WorldMatrix, out var Scale, out var NewRotation, out var Translation);

		Rotation = NewRotation;
	}
}
