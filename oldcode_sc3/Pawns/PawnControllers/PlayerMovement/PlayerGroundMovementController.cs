﻿using System;
using System.Numerics;
using Sandbox;
using Sandbox.UI;
using SpaceCombat3.Entities;
using SpaceCombat3.Interfaces;

// ReSharper disable MemberCanBePrivate.Global

namespace SpaceCombat3.Pawns.PawnControllers.PlayerMovement;

public partial class PlayerGroundMovementController : BasePlayerController, IGravity
{
	[Net]
	public float SprintSpeed
	{
		get;
		set;
	} = 650.0f;

	[Net]
	public float WalkSpeed
	{
		get;
		set;
	} = 200.0f;

	[Net]
	public float DefaultSpeed
	{
		get;
		set;
	} = 400.0f;

	[Net]
	public float Acceleration
	{
		get;
		set;
	} = 1000.0f;
	
	[Net]
	public float JumpVelocity
	{
		get;
		set;
	} = 500.0f;

	[Net]
	public float AirAcceleration
	{
		get;
		set;
	} = 50.0f;

	[Net]
	public float GroundFriction
	{
		get;
		set;
	} = 4.0f;

	[Net]
	public float GroundAngle
	{
		get;
		set;
	} = 46.0f;

	[Net]
	public float StepSize
	{
		get;
		set;
	} = 18.0f;

	[Net]
	public float BodyGirth
	{
		get;
		set;
	} = 32.0f;

	[Net]
	public float BodyHeight
	{
		get;
		set;
	} = 72.0f;

	[Net]
	public float EyeHeight
	{
		get;
		set;
	} = 64.0f;

	[Net]
	public float Gravity
	{
		get;
		set;
	} = 0f;

	[Net]
	public float AirControl
	{
		get;
		set;
	} = 30.0f;

	public Unstuck Unstuck;

	[Net]
	public Vector3 GravityDirection
	{
		get;
		set;
	} = Vector3.Down;

	public Matrix4x4 WorldMatrix = Matrix4x4.Identity;
	
	protected Rotation DeltaInputRotation = Rotation.Identity;
	protected Rotation LastInputRotation = Rotation.Identity;

	public PlayerGroundMovementController()
	{
		Unstuck = new Unstuck(this);

		GravityManager.RegisterRecipient(this);
	}

	~PlayerGroundMovementController()
	{
		GravityManager.UnregisterRecipient(this);
	}

	/// <summary>
	/// This is temporary, get the hull size for the player's collision
	/// </summary>
	public override BBox GetHull()
	{
		var Girth = BodyGirth * 0.5f;
		var Min = new Vector3(-Girth, -Girth, 0);
		var Max = new Vector3(+Girth, +Girth, BodyHeight);

		return new BBox(Min, Max);
	}


	// Duck body height 32
	// Eye Height 64
	// Duck Eye Height 28

	protected Vector3 Min;
	protected Vector3 Max;

	public virtual void SetBBox(Vector3 Min, Vector3 Max)
	{
		if (this.Min == Min && this.Max == Max)
			return;

		this.Min = Min;
		this.Max = Max;
	}

	/// <summary>
	/// Update the size of the bbox. We should really trigger some shit if this changes.
	/// </summary>
	public virtual void UpdateBBox()
	{
		var Girth = BodyGirth * 0.5f;

		var Min = new Vector3(-Girth, -Girth, 0) * Pawn.Scale;
		var Max = new Vector3(+Girth, +Girth, BodyHeight) * Pawn.Scale;

		SetBBox(Min, Max);
	}

	protected float SurfaceFriction;


	public virtual void UpdateEyeRotation()
	{
		EyeRotation = Rotation * Rotation.FromPitch(Input.Rotation.Pitch());
	}

	public virtual void DrawDebugInfo()
	{
		if (Debug)
		{
			var Transform = new Transform(Position, Rotation);
			var LocalVelocity = Transform.NormalToLocal(Velocity);
			
			DebugOverlay.Box(Position + TraceOffset, Min, Max, Color.Red);
			DebugOverlay.Box(Position, Min, Max, Color.Blue);

			var LineOffset = 0;
			if (Host.IsServer) LineOffset = 10;
			
			DebugOverlay.ScreenText($"        Position: {Position}", LineOffset + 0);
			DebugOverlay.ScreenText($"        Velocity: {Velocity}", LineOffset + 1);
			DebugOverlay.ScreenText($"    BaseVelocity: {BaseVelocity}", LineOffset + 2);
			DebugOverlay.ScreenText($"    GroundEntity: {GroundEntity} [{GroundEntity?.Velocity}]", LineOffset + 3);
			DebugOverlay.ScreenText($" SurfaceFriction: {SurfaceFriction}", LineOffset + 4);
			DebugOverlay.ScreenText($"   LocalVelocity: {LocalVelocity}", LineOffset + 5);
			DebugOverlay.ScreenText($"        Rotation: {Rotation}", LineOffset + 6);
			DebugOverlay.ScreenText($"      GravityDir: {GetGravityDirection(true)}", LineOffset + 7);
		}
	}

	public override void FrameSimulate()
	{
		UpdatePlayerRotation();
		UpdateEyeRotation();
		
		DrawDebugInfo();
	}

	public override void Simulate()
	{
		var GravityDir = GetGravityDirection(true);

		UpdatePlayerRotation();

		EyeLocalPosition = Vector3.Up * (EyeHeight * Pawn.Scale);
		UpdateBBox();

		EyeLocalPosition += TraceOffset;

		UpdateEyeRotation();

		Velocity += GravityDir * Gravity * 0.5f * Time.Delta;

		var Transform = new Transform(Position, Rotation);
		var LocalVelocity = Transform.NormalToLocal(Velocity);

		DoWalkingMovement(ref LocalVelocity);

		Velocity = Transform.NormalToWorld(LocalVelocity);

		Velocity += GravityDir * Gravity * 0.5f * Time.Delta;

		Move(Velocity * Time.Delta);

		GroundEntity = null;
		BaseVelocity = Vector3.Zero;

		DrawDebugInfo();
	}

	public virtual void DoWalkingMovement(ref Vector3 LocalVelocity)
	{
		var AccelerationDir = (Vector3.OneX * Input.Forward) + (Vector3.OneY * Input.Left);

		if (!AccelerationDir.IsNearZeroLength)
		{
			var WantedSpeed = GetWishSpeed();
			
			LocalVelocity =
				(LocalVelocity.WithZ(0f) + (AccelerationDir * Acceleration * Time.Delta)).Clamp(-WantedSpeed,
					WantedSpeed) + new Vector3(0f, 0f, LocalVelocity.z);
		}

		if (AccelerationDir.x.AlmostEqual(0))
		{
			LocalVelocity.x = LocalVelocity.x.Approach(0, Acceleration * Time.Delta);
		}

		if (AccelerationDir.y.AlmostEqual(0))
		{
			LocalVelocity.y = LocalVelocity.y.Approach(0, Acceleration * Time.Delta);
		}

		if (Input.Pressed(InputButton.Jump))
		{
			Jump(ref LocalVelocity);
		}
	}

	public virtual void Jump(ref Vector3 LocalVelocity)
	{
		//ClearGroundEntity();
		
		float StartZ = LocalVelocity.z;

		LocalVelocity = LocalVelocity.WithZ(StartZ + JumpVelocity);
		LocalVelocity += new Vector3(0, 0, Gravity * 0.5f) * Time.Delta;

		AddEvent("jump");
	}

	public virtual float GetWishSpeed()
	{
		if (Input.Down(InputButton.Run)) return SprintSpeed;
		if (Input.Down(InputButton.Walk)) return WalkSpeed;

		return DefaultSpeed;
	}

	Matrix4x4 MakeFromZX(Vector3 ZAxis, Vector3 XAxis)
	{
		Vector3 NewZ = ZAxis.Normal;
		Vector3 Norm = XAxis.Normal;

		// if they're almost same, we need to find arbitrary vector
		if (MathF.Abs(NewZ.Dot(Norm)).AlmostEqual(1f))
		{
			// make sure we don't ever pick the same as NewX
			Norm = (MathF.Abs(NewZ.z) < (1f - 0.0001)) ? Vector3.OneZ : Vector3.OneX;
		}

		Vector3 NewY = NewZ.Cross(Norm).Normal;
		Vector3 NewX = NewY.Cross(NewZ);

		return new Matrix4x4(
			NewX.x, NewX.y, NewX.z, 0f,
			NewY.x, NewY.y, NewY.z, 0f,
			NewZ.x, NewZ.y, NewZ.z, 0f,
			0f, 0f, 0f, 1f
		);
	}

	private void UpdatePlayerRotation()
	{
		Vector3 DesiredUpDir = GetGravityDirection(true) * -1.0f;

		// Update up direction if we're not close enough
		if (GetCapsuleAxisZ().Dot(DesiredUpDir) < 0.999999f)
		{
			WorldMatrix = MakeFromZX(DesiredUpDir, GetCapsuleAxisX());
			Matrix4x4.Decompose(WorldMatrix, out var Scale, out var NewRotation, out var Translation);

			Rotation = NewRotation;
		}
		
		// Update character yaw direction
		DeltaInputRotation = Rotation.Difference(LastInputRotation, Input.Rotation);
		Rotation *= Rotation.FromYaw(DeltaInputRotation.Yaw());
		LastInputRotation = Input.Rotation;
	}

	public void Move(Vector3 Delta, int Iterations = 0)
	{
		if (Iterations > 4)
			return;

		var Length = Delta.Length;
		var TargetPosition = Position + Delta;
		var TraceResult = Trace.Sphere(48, Position, Position + (Velocity * Time.Delta))
			.HitLayer(CollisionLayer.All, false)
			.HitLayer(CollisionLayer.Solid, true)
			.HitLayer(CollisionLayer.GRATE, true)
			.HitLayer(CollisionLayer.PLAYER_CLIP, true)
			.HitLayer(CollisionLayer.WINDOW, true)
			.HitLayer(CollisionLayer.NPC, true)
			.Ignore(Pawn)
			.Run();

		if (TraceResult.StartedSolid)
		{
			Position = TargetPosition;
		}
		else if (TraceResult.Hit)
		{
			Position = TraceResult.EndPosition + TraceResult.Normal;

			// subtract the normal from our velocity
			Velocity = Velocity.SubtractDirection(TraceResult.Normal);

			Delta = Velocity.Normal * Delta.Length * (1.0f - TraceResult.Fraction);

			Move(Delta, ++Iterations);
		}
		else
		{
			Position = TraceResult.EndPosition;
		}
	}

	Vector3 GetCapsuleAxisX()
	{
		// Fast simplification of FQuat::RotateVector() with FVector(1,0,0).
		Rotation CapsuleRotation = Rotation;
		Vector3 QuatVector = new Vector3(CapsuleRotation.x, CapsuleRotation.y, CapsuleRotation.z);

		return new Vector3(MathF.Pow(CapsuleRotation.w, 2f) - QuatVector.LengthSquared,
			CapsuleRotation.z * CapsuleRotation.w * 2.0f,
			CapsuleRotation.y * CapsuleRotation.w * -2.0f) + QuatVector * (CapsuleRotation.x * 2.0f);
	}

	Vector3 GetCapsuleAxisZ()
	{
		// Fast simplification of FQuat::RotateVector() with FVector(0,0,1).
		Rotation CapsuleRotation = Rotation;
		Vector3 QuatVector = new Vector3(CapsuleRotation.x, CapsuleRotation.y, CapsuleRotation.z);

		return new Vector3(CapsuleRotation.y * CapsuleRotation.w * 2.0f, CapsuleRotation.x * CapsuleRotation.w * -2.0f,
			MathF.Pow(CapsuleRotation.w, 2) - QuatVector.LengthSquared) + QuatVector * (CapsuleRotation.z * 2.0f);
	}

	Vector3 GetGravityDirection(bool AvoidZeroGravity)
	{
		// Gravity direction can be influenced by the custom gravity scale value.
		if (Gravity != 0.0f)
		{
			if (!GravityDirection.IsNearZeroLength)
			{
				return GravityDirection * ((Gravity > 0.0f) ? 1.0f : -1.0f);
			}

			if (AvoidZeroGravity)
			{
				return Vector3.Down;
			}
		}
		else if (AvoidZeroGravity)
		{
			if (!GravityDirection.IsNearZeroLength)
			{
				return GravityDirection;
			}

			return Vector3.Down;
		}

		return Vector3.Zero;
	}

	public void ApplyGravity(Vector3 GravityDirection, float GravityStrength)
	{
		if (GravityStrength <= 1f)
		{
			this.Gravity = 0f;
			return;
		}

		this.GravityDirection = GravityDirection;
		this.Gravity = GravityStrength;
	}

	public Vector3 GetGravityPosition()
	{
		return Position;
	}
}
