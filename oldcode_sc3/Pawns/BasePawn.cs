﻿using Sandbox;
using SpaceCombat3.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace SpaceCombat3.Pawns;

public partial class BasePawn : AnimatedEntity, IDamageInterface
{
	public override void Simulate(Client cl)
	{
		var TheController = GetActiveController();
		TheController?.Simulate(cl, this, GetActiveAnimator());
	}

	public bool CanDamage(Entity Instigator)
	{
		throw new System.NotImplementedException();
	}

	public override void FrameSimulate(Client cl)
	{
		base.FrameSimulate(cl);

		var TheController = GetActiveController();
		TheController?.FrameSimulate(cl, this, GetActiveAnimator());
	}

	public override void Spawn()
	{
		EnableLagCompensation = true;

		Tags.Add("player");

		base.Spawn();
	}

	/// <summary>
	/// Called once the player's health reaches 0
	/// </summary>
	public override void OnKilled()
	{
		Game.Current?.OnKilled(this);

		LifeState = LifeState.Dead;
		StopUsing();

		Client?.AddInt("deaths", 1);
	}

	/// <summary>
	/// Called from the gamemode, clientside only.
	/// </summary>
	public override void BuildInput(InputBuilder input)
	{
		if (input.StopProcessing)
			return;

		ActiveChild?.BuildInput(input);

		GetActiveController()?.BuildInput(input);

		if (input.StopProcessing)
			return;

		GetActiveAnimator()?.BuildInput(input);
	}

	/// <summary>
	/// Called after the camera setup logic has run. Allow the player to
	/// do stuff to the camera, or using the camera. Such as positioning entities
	/// relative to it, like viewmodels etc.
	/// </summary>
	public override void PostCameraSetup(ref CameraSetup setup)
	{
		Host.AssertClient();

		if (ActiveChild != null)
		{
			ActiveChild.PostCameraSetup(ref setup);
		}
	}

	public override void StartTouch(Entity other)
	{
		if (IsClient) return;

		if (other is PickupTrigger)
		{
			StartTouch(other.Parent);
			return;
		}

		Inventory?.Add(other, Inventory.Active == null);
	}

	public override void TakeDamage(DamageInfo info)
	{
		if (LifeState == LifeState.Dead)
			return;

		base.TakeDamage(info);

		//this.ProceduralHitReaction(info);

		//
		// Add a score to the killer
		//
		if (LifeState == LifeState.Dead && info.Attacker != null)
		{
			if (info.Attacker.Client != null && info.Attacker != this)
			{
				info.Attacker.Client.AddInt("kills");
			}
		}
	}
}
