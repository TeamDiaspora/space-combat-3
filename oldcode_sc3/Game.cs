﻿using System;
using System.Linq;
using Sandbox;
using SpaceCombat3.Entities;
using SpaceCombat3.Pawns;
using SpaceCombat3.Pawns.Vehicles;
using SpaceCombat3.Voxels;
using SpaceCombat3.Voxels.Generators;
using SpaceCombat3.Voxels.Volumes;
using TinkerWorX.AccidentalNoiseLibrary;
using Prop = SpaceCombat3.Entities.Prop;

//
// You don't need to put things in a namespace, but it doesn't hurt.
//
namespace SpaceCombat3
{
	/// <summary>
	/// This is your game class. This is an entity that is created serverside when
	/// the game starts, and is replicated to the client. 
	/// 
	/// You can use this to create things like HUDs and declare which player class
	/// to use for spawned players.
	/// </summary>
	public partial class MyGame : Sandbox.Game
	{
		public static GeneratedVoxelVolume TestVolume = null;
		public GravityManager GravityManager = null;

		public MyGame()
		{
			Map.Physics.Gravity = Vector3.Zero;
			Map.Physics.AirDensity = 0;

			GravityManager = new GravityManager();

			if (Map.Name == "sb_empty")
			{
				SpawnVoxelVolume(Vector3.Zero);
			}
		}

		/// <summary>
		/// A client has joined the server. Make them a pawn to play with
		/// </summary>
		public override void ClientJoined(Client client)
		{
			base.ClientJoined(client);

			// Create a pawn for this client to play with
			var Pawn = new PlayerPawn(client);//new SpaceShipPawn();
			//MoveToSpawnpoint(Pawn);
			client.Pawn = Pawn;
		}

		public override void DoPlayerNoclip(Client player)
		{
			if (player.Pawn is BasePawn PlayerPawn)
			{
				if (PlayerPawn.DevController is NoclipController)
				{
					Log.Info("Noclip Mode Off");
					PlayerPawn.DevController = null;
				}
				else
				{
					Log.Info("Noclip Mode On");
					PlayerPawn.DevController = new NoclipController();
				}
			}
		}
		
		string modelToShoot = "models/citizen_props/crate01.vmdl";
		void ShootBox(Client cl)
		{
			var ent = new Prop
			{
				Position = cl.Pawn.Position + cl.Pawn.Rotation.Forward * 1000,
				Rotation = cl.Pawn.Rotation
			};

			ent.SetModel( modelToShoot );
			ent.Velocity = cl.Pawn.Rotation.Forward * 1600;
		}

		[ConCmd.Server("sc_reset_planets")]
		public static void ResetPlanets()
		{
			if (Map.Name == "sb_empty")
			{
				SpawnVoxelVolume(Vector3.Zero);
			}
		}

		public static void SpawnVoxelVolume(Vector3 Position)
		{
			TestVolume?.Delete();

			TestVolume = new PlanetVoxelVolume(16384, 1f, 1f);
			TestVolume.Position = Position;

			var Generator = new PlanetVoxelGenerator
			{
				Fractals = new[]
				{
					new FractalInfo(FractalType.Billow, 0.025f, 2, 0.93f, 0.1f),
					new FractalInfo(FractalType.FractionalBrownianMotion, 0.1f, 2, 0.95f, 0.5f),
					new FractalInfo(FractalType.RidgedMulti, 0.01f, 4, 0.94f, 0.25f)
				},
				Terrains = new[]
				{
					new TerrainInfo(0, 1f, 0.95f, true, 1),
					new TerrainInfo(2, 1f, 0.95f, true, 1, true, 2, 0.08f, 0.85f)
				},
				Volume = TestVolume
			};

			TestVolume.Generator = Generator;
			TestVolume.GenerateVolume();
		}
		
		public override void Simulate(Client cl)
		{
			base.Simulate(cl);

			if (Map.Name != "sb_empty")
			{
				if (Input.Pressed(InputButton.SecondaryAttack) && IsAuthority)
				{
					SpawnVoxelVolume(cl.Pawn.Position + cl.Pawn.Rotation.Forward * 16000);
				}
			}

			if (Input.Pressed(InputButton.PrimaryAttack) && IsAuthority)
			{
				ShootBox(cl);
			}
		}

		[ConCmd.Admin("respawn_entities")]
		public static void RespawnEntities()
		{
			Map.Reset(DefaultCleanupFilter);
		}
	}
}
