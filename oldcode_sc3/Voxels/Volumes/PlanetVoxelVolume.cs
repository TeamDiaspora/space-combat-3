﻿using System;
using Sandbox;
using SpaceCombat3.Entities;
using SpaceCombat3.Interfaces;

namespace SpaceCombat3.Voxels.Volumes;

public partial class PlanetVoxelVolume : GeneratedVoxelVolume, IGravitySource
{
	[Net]
	protected float GravityRadius
	{
		get;
		set;
	}

	[Net]
	protected float Gravity
	{
		get;
		set;
	}

	[Net]
	protected float Radius
	{
		get;
		set;
	}

	public PlanetVoxelVolume() : base()
	{
		
	}
	
	public PlanetVoxelVolume(float PlanetRadius, float GravityMultiplier = 1f, float ChunkScale = 1f) : base(PlanetRadius * 2, PlanetRadius * 0.25f * ChunkScale, 4,
		NormalStyle.Smooth, true)
	{
		Radius = PlanetRadius;
		GravityRadius = (Radius * GravityMultiplier);
		Gravity = 800f * GravityMultiplier;
	}

	public override void Spawn()
	{
		base.Spawn();
		
		GravityManager.RegisterSource(this);
	}
	
	public override void ClientSpawn()
	{
		base.ClientSpawn();
		
		GravityManager.RegisterSource(this);
	}

	protected override void OnDestroy()
	{
		base.OnDestroy();
		
		GravityManager.UnregisterSource(this);
	}

	public float GetGravityRadius()
	{
		return Radius + GravityRadius;
	}

	public Vector3 GetGravityCenter()
	{
		return Position;
	}

	public bool ShouldAffectGravityRecipient(IGravity Recipient)
	{
		return true;
	}

	public void CalculateGravityForRecipient(IGravity Recipient, out float GravityStrength, out Vector3 GravityDirection)
	{
		var RadiusSq = Radius * Radius; 
		GravityDirection = (Position - Recipient.GetGravityPosition()).Normal;
		GravityStrength = Math.Max(1f - ((Recipient.GetGravityPosition().DistanceSquared(Position) - RadiusSq) / (GravityRadius * GravityRadius)), 0f) * Gravity;
	}
}
