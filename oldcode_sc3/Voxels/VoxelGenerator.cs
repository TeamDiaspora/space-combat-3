﻿namespace SpaceCombat3.Voxels;

public class VoxelGenerator
{
	protected Vector3i PositionOffset;
	public VoxelVolume Volume;
	
	public virtual void GenerateChunk(Vector3i ChunkPos, VoxelChunk Chunk)
	{
		PositionOffset = ChunkPos;
		Chunk.Data.GenerateFromGenerator(this);
	}

	public virtual void GenerateVoxel(float X, float Y, float Z, ref Voxel Voxel)
	{
		
	}
}

