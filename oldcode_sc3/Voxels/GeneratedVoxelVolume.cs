﻿using Sandbox;
using SpaceCombat3.Voxels.Generators;

namespace SpaceCombat3.Voxels;

public class GeneratedVoxelVolume : VoxelVolume
{
	protected readonly int MaxChunksPerGeneration = 5;
	protected readonly int FramesBetweenGenerations = 15;

	public VoxelGenerator Generator;

	protected bool IsGeneratingChunks = false;
	protected Vector3i[] ChunkQueue;
	protected int LastChunkGenerated = 0;
	protected int FramesSinceLastGeneration = 0;


	public GeneratedVoxelVolume() : base()
	{
	}

	public GeneratedVoxelVolume(Vector3 Size, float ChunkSize, int ChunkSubdivisions = 4,
		NormalStyle NormalStyle = NormalStyle.Smooth, bool ShouldOverrideChunkRendering = false) :
		base(Size, ChunkSize, ChunkSubdivisions, NormalStyle, ShouldOverrideChunkRendering)
	{
	}

	[Event.Tick]
	public void Tick()
	{
		if (IsAuthority && IsGeneratingChunks)
		{
			if (FramesSinceLastGeneration >= FramesBetweenGenerations)
			{
				FramesSinceLastGeneration = 0;

				var NumChunksGenerated = 0;
				while (NumChunksGenerated < MaxChunksPerGeneration && LastChunkGenerated < ChunkQueue.Length)
				{
					GetOrCreateChunk(ChunkQueue[LastChunkGenerated]);
					++NumChunksGenerated;
					++LastChunkGenerated;
				}
			}
			else
			{
				++FramesSinceLastGeneration;
			}
		}
	}

	public virtual void GenerateVolume()
	{
		Log.Info("Starting generation for voxel volume");
		var ChunkCount = Vector3i.Ceiling(LocalSize * (1f / ChunkSize));

		ChunkQueue = new Vector3i[ChunkCount.x * ChunkCount.y * ChunkCount.z];
		IsGeneratingChunks = true;

		foreach (var (Index3, Index1) in ChunkCount.EnumerateArray3D(Vector3i.Zero, ChunkCount))
		{
			ChunkQueue[Index1] = Index3;
		}

		Log.Info($"Queued {ChunkQueue.Length} chunks for generation");
	}

	protected override void OnChunkCreated(Vector3i ChunkPos, ref VoxelChunk NewChunk)
	{
		Log.Info($"Generating chunk {NewChunk} with offset {ChunkPos}");
		Generator.GenerateChunk(ChunkPos, NewChunk);

		Log.Info($"Updating mesh for chunk {NewChunk}");
		NewChunk.InvalidateMesh();

		Log.Info($"Finished generating chunk {NewChunk}");
	}
}
