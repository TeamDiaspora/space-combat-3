﻿using System;
using TinkerWorX.AccidentalNoiseLibrary;

namespace SpaceCombat3.Voxels.Generators;

public class PlanetVoxelGenerator : FractalVoxelGenerator
{
	private readonly float RadiusScale = 0.95f;
	
	protected override void SetupTerrainCache()
	{
		base.SetupTerrainCache();
		
		var ChunkCount = Vector3i.Ceiling(Volume.LocalSize * (1f / Volume.ChunkSize));

		var HalfSub = (1 << Volume.ChunkSubdivisions) * 0.5f;
		var XCenter = (ChunkCount.x) * HalfSub;
		var YCenter = (ChunkCount.y) * HalfSub;
		var ZCenter = (ChunkCount.z) * HalfSub;
		var Radius = RadiusScale * HalfSub * Math.Min(ChunkCount.x, Math.Min(ChunkCount.y, ChunkCount.z));
		
		Log.Info($"Creating new planet terrain with {(1 << Volume.ChunkSubdivisions)} subdivisions at center {XCenter}, {YCenter}, {ZCenter} and radius {Radius}");
		
		var Sphere = new ImplicitSphere(XCenter, YCenter, ZCenter, 0, 0, 0, Radius);
		var SphereRadius = new ImplicitCombiner(CombinerType.Multiply);
		SphereRadius.AddSource(new ImplicitConstant(Radius));
		SphereRadius.AddSource(GeneratorModule);
		
		Sphere.Radius = SphereRadius;
		
		GeneratorModule = Sphere;

	}
}
