﻿using System;
using TinkerWorX.AccidentalNoiseLibrary;

namespace SpaceCombat3.Voxels.Generators;

public struct TerrainInfo
{
	public int MainFractalId = 0;
	public float Scale = 0.06f;
	public float Offset = 0.95f;
	public bool UseTranslateDomain = false;
	public int TranslateDomainFractalId = 0;
	public float Falloff = 0.15f;
	public float Threshold = 0.5f;
	public int SelectFractalId = 0;

	public TerrainInfo()
	{
	}

	public TerrainInfo(int MainFractalId, float Scale, float Offset, bool UseTranslateDomain = false, int TranslateDomainFractalId = 0,
		bool UseSelect = false, int SelectFractalId = 0, float SelectFalloff = 0, float SelectThreshold = 0)
	{
		this.MainFractalId = MainFractalId;
		this.Scale = Scale;
		this.Offset = Offset;
		this.UseTranslateDomain = UseTranslateDomain;
		this.TranslateDomainFractalId = TranslateDomainFractalId;
		this.Falloff = SelectFalloff;
		this.Threshold = SelectThreshold;
		this.SelectFractalId = SelectFractalId;
	}
};

public struct FractalInfo
{
	public FractalType Type;
	public float Frequency = 1;
	public int Octaves = 5;
	public float Offset = 0.95f;
	public float Scale = 0.06f;

	public FractalInfo()
	{
		Type = FractalType.FractionalBrownianMotion;
	}

	public FractalInfo(FractalType Type, float Frequency, int Octaves, float Offset, float Scale)
	{
		this.Type = Type;
		this.Frequency = Frequency;
		this.Octaves = Octaves;
		this.Offset = Offset;
		this.Scale = Scale;
	}
};

public class CachedFractal
{
	public CachedFractal(FractalType Type, int Seed, int Octaves, float Frequency, float Scale, float Offset)
	{
		// Configure the fractal
		Fractal = new ImplicitFractal(Type, BasisType.Simplex, InterpolationType.None);
		Fractal.Frequency = Frequency;
		Fractal.Octaves = Octaves;
		Fractal.Seed = Seed;
		
		// Scale and offset the fractal
		ScaleOffset = new ImplicitScaleOffset(Fractal, Scale, Offset);
		
		// Cache for performance
		Cache = new ImplicitCache(ScaleOffset);
	}

	protected ImplicitFractal Fractal;
	protected ImplicitScaleOffset ScaleOffset;
	public ImplicitCache Cache;
}

public class CachedTerrain
{
	public CachedTerrain(CachedFractal Main, CachedFractal TranslateDomainF, CachedFractal SelectF, ref ImplicitCache OtherSelect, float Scale, float Offset, bool UseTranslateDomain = false, bool UseSelect = false, float SelectFalloff = 0, float SelectThreshold = 0)
	{
		ScaleOffset = new ImplicitScaleOffset(Main.Cache, Scale, Offset);
		
		if (UseSelect)
		{
			if (UseTranslateDomain)
			{
				TranslateDomain = new ImplicitTranslateDomain(ScaleOffset, TranslateDomainF.Cache, TranslateDomainF.Cache, TranslateDomainF.Cache);
				Select = new ImplicitSelect(SelectF.Cache, OtherSelect, TranslateDomain, SelectFalloff, SelectThreshold);
			}
			else
			{
				Select = new ImplicitSelect(SelectF.Cache, OtherSelect, ScaleOffset, SelectFalloff, SelectThreshold);
			}

			Cache = new ImplicitCache(Select);
		}
		else if (UseTranslateDomain)
		{
			TranslateDomain = new ImplicitTranslateDomain(ScaleOffset, TranslateDomainF.Cache, TranslateDomainF.Cache, TranslateDomainF.Cache);
			Cache = new ImplicitCache(TranslateDomain);
		}
		else
		{
			Cache = new ImplicitCache(ScaleOffset);
		}
	}

	protected ImplicitScaleOffset ScaleOffset;
	protected ImplicitTranslateDomain TranslateDomain;
	protected ImplicitSelect Select;
	
	public ImplicitCache Cache;
}

public class FractalVoxelGenerator : VoxelGenerator
{
	public int Seed;
	public FractalInfo[] Fractals;
	public TerrainInfo[] Terrains;

	protected CachedFractal[] FractalCache;
	protected CachedTerrain[] TerrainCache;
	protected ImplicitModuleBase GeneratorModule = null;
	
	protected virtual void SetupFractalCache()
	{
		FractalCache = new CachedFractal[Fractals.Length];
		for (var Index = 0; Index < Fractals.Length; Index++)
		{
			var Info = Fractals[Index];
			FractalCache[Index] = new CachedFractal(Info.Type, Seed, Info.Octaves, Info.Frequency, Info.Scale, Info.Offset);
		}
	}
	
	protected virtual void SetupTerrainCache()
	{
		TerrainCache = new CachedTerrain[Terrains.Length];
		CachedTerrain LastTerrain = null;
		for (var Index = 0; Index < Terrains.Length; Index++)
		{
			var Info = Terrains[Index];

			// First terrain can't use select
			if (Index == 0)
			{
				LastTerrain = new CachedTerrain(FractalCache[Info.MainFractalId], FractalCache[Info.TranslateDomainFractalId],
					FractalCache[Info.MainFractalId], ref FractalCache[Info.MainFractalId].Cache, Info.Scale, Info.Offset,
					Info.UseTranslateDomain);
			}
			else
			{
				LastTerrain = new CachedTerrain(FractalCache[Info.MainFractalId], FractalCache[Info.TranslateDomainFractalId],
					FractalCache[Info.SelectFractalId], ref LastTerrain.Cache, Info.Scale, Info.Offset,
					Info.UseTranslateDomain, true, Info.Falloff, Info.Threshold);
			}
		}

		GeneratorModule = LastTerrain.Cache;
	}

	public override void GenerateChunk(Vector3i ChunkPos, VoxelChunk Chunk)
	{
		if (GeneratorModule == null)
		{
			SetupFractalCache();
			SetupTerrainCache();
		}

		base.GenerateChunk(ChunkPos, Chunk);
	}
	
	public override void GenerateVoxel(float X, float Y, float Z, ref Voxel Voxel)
	{
		Voxel.RawValue = (byte)(Math.Clamp(GeneratorModule.Get(X + PositionOffset.x * 16, Y + PositionOffset.y * 16, Z + PositionOffset.z * 16), 0, 1) * 255);
	}
}
